<?php

class BrandColor extends DataObject {

    private static $singular_name = 'Brand color';
    private static $plural_name = 'Brand colors';

    static $db = array(
        'NameInLesscss' => 'Varchar(50)',
        'NameInCMS' => 'Varchar(50)',
        'ColorHex' => 'Varchar(20)',
        'ColorRgb' => 'Varchar(20)'
    );

    private static $many_many = array(
        'BrandColorTypes' => 'BrandColorType'
    );

    private static $has_one = array(
        'SiteConfig' => 'SiteConfig'
    );

    private static $summary_fields = array(
        'getColor',
        'ColorHex',
        'NameInLesscss',
        'NameInCMS'
    );

    public function getColor(){

        $output = '<div style="width:50px;height:50px;border:1px solid #333;background-color:'.$this->ColorHex.';"></div>';

        return $output;

    }

    function fieldLabels($includerelations = true) {
        $labels = parent::fieldLabels($includerelations);

        $labels['NameInLesscss'] = _t("BrandColor.NAMEINLESSCSS", "Name in LESS/CSS");
        $labels['NameInCMS'] = _t("BrandColor.NAMEINCMS", "Name in CMS");
        $labels['ColorHex'] = _t("BrandColor.COLORHEX", "Color HEX");
        $labels['ColorRgb'] = _t("BrandColor.COLORRGB", "Color RGB");

        return $labels;
    }

    public function getCMSFields() {

        // Field labels
        $l = $this->fieldLabels();

        $BrandColorTypesList = BrandColorType::get()->map('ID', 'ColorType')->toArray();

        $NameInLesscssField = TextField::create('NameInLesscss', $l['NameInLesscss']);
        $NameInCMSField = TextField::create('NameInCMS', $l['NameInCMS']);
        $ColorHexField = TextField::create('ColorHex', $l['ColorHex']);
        $ColorRgbField = TextField::create('ColorRgb', $l['ColorRgb']);
        $BrandColorTypesField = ListboxField::create('BrandColorTypes', 'BrandColorTypes', $BrandColorTypesList, $value = 1, $size = 4, $multiple = true);

        $fields = new FieldList(
            $NameInLesscssField,
            $NameInCMSField,
            $ColorHexField,
            $ColorRgbField,
            $BrandColorTypesField
        );

        return $fields;

    }

    public function validate() {
        $result = parent::validate();
        if(!preg_match('/^#[A-Za-z0-9]{6}$/i', $this->ColorHex)) {
            $result->error(_t('BrandColor.INVALID_COLORHEX','This is not a valid HEX color.'));
        }
        if(!preg_match('/^[A-Za-z]{1}[A-Za-z0-9\\-]{1,}$/i', $this->NameInLesscss)) {
            $result->error(_t('BrandColor.INVALID_NAMEINLESSCSS','This is not a valid name, use only letters, numbers and dashes and start with a letter.'));
        }
        if(!preg_match('/^[A-Za-z]{1}[A-Za-z0-9\\-]{1,}$/i', $this->NameInCMS)) {
            $result->error(_t('BrandColor.INVALID_NAMEINCMS','This is not a valid name, use only letters, numbers and dashes and start with a letter'));
        }
        return $result;
    }

    /*public function canView($member=null) {
        return Permission::check('ADMIN') || Permission::check('CMS_ACCESS_PaymentManagerAdmin') || Permission::check('CMS_ACCESS_LeftAndMain') ;
    }

    public function canEdit($member=null) {
        return Permission::check('ADMIN') || Permission::check('CMS_ACCESS_PaymentManagerAdmin') || Permission::check('CMS_ACCESS_LeftAndMain') ;
    }

    public function canCreate($member=null) {
        return Permission::check('ADMIN') || Permission::check('CMS_ACCESS_PaymentManagerAdmin') || Permission::check('CMS_ACCESS_LeftAndMain') ;
    }

    public function canDelete($member=null) {
        return Permission::check('ADMIN') || Permission::check('CMS_ACCESS_PaymentManagerAdmin') || Permission::check('CMS_ACCESS_LeftAndMain') ;
    }*/

}