<?php

class BrandColorType extends DataObject {

    private static $singular_name = 'Brand color type';
    private static $plural_name = 'Brand color typen';

    static $db = array(
        'ColorType' => 'Varchar(20)',
        'Description' => 'Varchar(50)'
    );

    private static $has_one = array(
        'SiteConfig' => 'SiteConfig'
    );

    private static $belongs_many_many = array(
        'BrandColors' => 'BrandColor',
    );

    private static $summary_fields = array(
        'ColorType',
        'Description'
    );

    function fieldLabels($includerelations = true) {
        $labels = parent::fieldLabels($includerelations);

        $labels['ColorType'] = _t("BrandColor.COLORTYPE", "Color type");
        $labels['Description'] = _t("BrandColor.DESCRIPTION", "Description");

        return $labels;
    }

    public function getCMSFields() {

        // Field labels
        $l = $this->fieldLabels();

        $ColorTypeField = TextField::create('ColorType', $l['ColorType']);
        $DescriptionField = TextField::create('Description', $l['Description']);

        $fields = new FieldList(
            $ColorTypeField,
            $DescriptionField
        );

        return $fields;

    }

    /*public function validate() {
        $result = parent::validate();
        if(!filter_var($this->IpAddress, FILTER_VALIDATE_IP)) {
            $result->error(_t('OwnIp.INVALID_IPADDRESS','This is not a valid ip address'));
        }
        if (!$this->ID && OwnIp::get()->filter('IpAddress', $this->IpAddress)->count() != 0){
            $result->error(_t('OwnIp.IPADDRESS_ALREADY_LISTED','This ip address is already in the list'));
        }
        return $result;
    }*/

    /*public function canView($member=null) {
        return Permission::check('ADMIN') || Permission::check('CMS_ACCESS_PaymentManagerAdmin') || Permission::check('CMS_ACCESS_LeftAndMain') ;
    }

    public function canEdit($member=null) {
        return Permission::check('ADMIN') || Permission::check('CMS_ACCESS_PaymentManagerAdmin') || Permission::check('CMS_ACCESS_LeftAndMain') ;
    }

    public function canCreate($member=null) {
        return Permission::check('ADMIN') || Permission::check('CMS_ACCESS_PaymentManagerAdmin') || Permission::check('CMS_ACCESS_LeftAndMain') ;
    }

    public function canDelete($member=null) {
        return Permission::check('ADMIN') || Permission::check('CMS_ACCESS_PaymentManagerAdmin') || Permission::check('CMS_ACCESS_LeftAndMain') ;
    }*/

}