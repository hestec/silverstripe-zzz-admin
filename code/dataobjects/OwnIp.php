<?php

class OwnIp extends DataObject {

    private static $singular_name = 'Own ip address';
    private static $plural_name = 'Own ip addresses';

    static $db = array(
        'IpAddress' => 'Varchar(20)',
        'Description' => 'Varchar(50)'
    );

    private static $has_one = array(
        'SiteConfig' => 'SiteConfig'
    );

    private static $summary_fields = array(
        'IpAddress',
        'Description'
    );

    function fieldLabels($includerelations = true) {
        $labels = parent::fieldLabels($includerelations);

        $labels['IpAddress'] = _t("OwnIp.IPADDRESS", "IP address");
        $labels['Description'] = _t("OwnIp.DESCRIPTION", "Description");

        return $labels;
    }

    public function getCMSFields() {

        // Field labels
        $l = $this->fieldLabels();

        $ipaddressfield = TextField::create('IpAddress', $l['IpAddress']);
        $ipaddressfield->setValue($_SERVER['REMOTE_ADDR']);
        if (!$this->ID){
            $ipaddressfield->setDescription(_t("OwnIp.YOUR_OWN_IP", "This is your current ip address, you may change this for another."));
        }

        $descriptionfield = TextField::create('Description', $l['Description']);
        $descriptionfield->setDescription(_t("OwnIp.DESCRIPTION_IS_OPTIONAL", "Description is optional"));

        GridFieldConfig::create()->addComponent(new GridFieldDeleteAction());

        $fields = new FieldList(
            $ipaddressfield,
            $descriptionfield
        );

        return $fields;

    }

    public function validate() {
        $result = parent::validate();
        if(!filter_var($this->IpAddress, FILTER_VALIDATE_IP)) {
            $result->error(_t('OwnIp.INVALID_IPADDRESS','This is not a valid ip address'));
        }
        if (!$this->ID && OwnIp::get()->filter('IpAddress', $this->IpAddress)->count() != 0){
            $result->error(_t('OwnIp.IPADDRESS_ALREADY_LISTED','This ip address is already in the list'));
        }
        return $result;
    }

    /*public function canView($member=null) {
        return Permission::check('ADMIN') || Permission::check('CMS_ACCESS_PaymentManagerAdmin') || Permission::check('CMS_ACCESS_LeftAndMain') ;
    }

    public function canEdit($member=null) {
        return Permission::check('ADMIN') || Permission::check('CMS_ACCESS_PaymentManagerAdmin') || Permission::check('CMS_ACCESS_LeftAndMain') ;
    }

    public function canCreate($member=null) {
        return Permission::check('ADMIN') || Permission::check('CMS_ACCESS_PaymentManagerAdmin') || Permission::check('CMS_ACCESS_LeftAndMain') ;
    }

    public function canDelete($member=null) {
        return Permission::check('ADMIN') || Permission::check('CMS_ACCESS_PaymentManagerAdmin') || Permission::check('CMS_ACCESS_LeftAndMain') ;
    }*/

}