<?php

class ExtraBlockExtension extends DataExtension {

    private static $db = array(
        'HeadingTitle' => 'Varchar(255)',
        'HeadingTitleType' => "Enum('hidden,h2,h3,h4,h5,h6', 'hidden')",
        'DisplayXS' => 'Boolean',
        'DisplaySM' => 'Boolean',
        'DisplayMD' => 'Boolean',
        'DisplayLG' => 'Boolean',
        'DisplayXL' => 'Boolean',
        'ListIcon' => "Enum('circle,check,arrow', 'circle')"
    );

    public static $defaults = array(
        'DisplayXS' => 1,
        'DisplaySM' => 1,
        'DisplayMD' => 1,
        'DisplayLG' => 1,
        'DisplayXL' => 1
    );

    public function updateCMSFields(FieldList $fields) {

        $TitleTypeSource = singleton('Block')->dbObject('HeadingTitleType')->enumValues();
        $ListIconSource = singleton('Block')->dbObject('ListIcon')->enumValues();

        $HeadingTitleField = TextField::create('HeadingTitle', _t('Block.HEADINGTITLE', "Heading Title"));
        $HeadingTitleTypeField = DropdownField::create('HeadingTitleType', _t('Block.HEADINGTITLETYPE', "Heading Title Type"), $TitleTypeSource);
        $ListIconField = DropdownField::create('ListIcon', _t('Block.LISTICON', "List icon"), $ListIconSource);

        $DisplayXSField = CheckboxField::create('DisplayXS', 'DisplayXS');
        $DisplaySMField = CheckboxField::create('DisplaySM', 'DisplaySM');
        $DisplayMDField = CheckboxField::create('DisplayMD', 'DisplayMD');
        $DisplayLGField = CheckboxField::create('DisplayLG', 'DisplayLG');
        $DisplayXLField = CheckboxField::create('DisplayXL', 'DisplayXL');


        $fields->addFieldsToTab('Root.Display', array(
            $DisplayXSField,
            $DisplaySMField,
            $DisplayMDField,
            $DisplayLGField,
            $DisplayXLField
        ));

        $fields->renameField('Title', _t('Block.TITLE_IN_CMS', "Title in CMS"));

        $fields->insertAfter($HeadingTitleField, 'Title');
        $fields->insertAfter($HeadingTitleTypeField, 'HeadingTitle');
        $fields->insertAfter($ListIconField, 'HeadingTitleType');



        //$fields->addFieldToTab("Root.Settings", new TreeDropdownField('InternLinkID', 'Intern Link', 'SiteTree'));
    }

}