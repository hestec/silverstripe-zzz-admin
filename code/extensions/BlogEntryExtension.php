<?php
class BlogEntryExtension extends DataExtension {

    private static $has_one = array(
        'ImpressionImage'=>'Image'
    );

    public function updateCMSFields(FieldList $fields) {
        //$fields = parent::getCMSFields();

        $fields->addFieldToTab('Root.Main', new UploadField('ImpressionImage', _t("SiteTree.IMPRESSIONIMAGE","Impression image")), 'Content');
        $fields->removeFieldFromTab('Root.Main', 'HeaderImage');

        return $fields;
    }

    public function ParentHeaderImage() {

        return $this->owner->Parent()->URLSegment;

    }

}