<?php

class CustomSiteConfig extends DataExtension {

    static $db = array(
        'FooterContent' => 'HTMLText',
        'GATrackingCode' => 'Varchar',
        'GlobalFromEmail' => 'Varchar(255)',
        'GlobalFromName' => 'Varchar(255)',
        'FacebookUrl' => 'Varchar(255)',
        'TwitterUrl' => 'Varchar(255)',
        'InstagramUrl' => 'Varchar(255)',
        'YoutubeUrl' => 'Varchar(255)',
        'LinkedinUrl' => 'Varchar(255)',
        'PinterestUrl' => 'Varchar(255)',
        'GoogleplusUrl' => 'Varchar(255)',
        'SnapchatUrl' => 'Varchar(255)',
        'SoundcloudUrl' => 'Varchar(255)',
        'FacebookAppId' => 'Varchar',
        'TawkSiteId' => 'Varchar',
        'FooterColumnTitle1' => 'Varchar',
        'FooterColumnTitle2' => 'Varchar',
        'FooterColumnTitle3' => 'Varchar',
        'FooterColumnTitle4' => 'Varchar',
        'FooterContent2' => 'HTMLText',
        'FooterContent3' => 'HTMLText',
        'FooterContent4' => 'HTMLText'
    );

    private static $has_many = array(
        'OwnIps' => 'OwnIp',
        'BrandColors' => 'BrandColor',
        'BrandColorTypes' => 'BrandColorType',
        'ExtraNavigationItems' => 'ExtraNavigationItem'
    );

    public function updateCMSFields(FieldList $fields)
    {

        if (!Permission::check('ADMIN')){
            $fields->removeByName(array(
                'Access'
            ));
            $fields->removeFieldFromTab('Root.Main', 'Theme');
        }

        $ExtraNavigationItemsField = new GridField(
            'ExtraNavigationItems',
            _t("SiteConfig.NAVIGATION_ITEMS", "Navigation items"),
            $this->owner->ExtraNavigationItems(),
            $config = GridFieldConfig::create()
                ->addComponent(new GridFieldToolbarHeader())
                ->addComponent(new GridFieldAddNewButton("toolbar-header-right"))
                ->addComponent(new GridFieldSortableHeader())
                ->addComponent(new GridFieldDataColumns())
                ->addComponent(new GridFieldPaginator(50))
                ->addComponent(new GridFieldEditButton())
                ->addComponent(new GridFieldDeleteAction())
                ->addComponent(new GridFieldDetailForm())
                ->addComponent(new GridFieldFilterHeader())
                ->addComponent(new GridFieldOrderableRows('SortOrder'))
        );

        $BrandColorsField = new GridField(
            'BrandColors',
            'BrandColors',
            $this->owner->BrandColors(),
            $config = GridFieldConfig::create()
                ->addComponent(new GridFieldToolbarHeader())
                ->addComponent(new GridFieldAddNewButton("toolbar-header-right"))
                ->addComponent(new GridFieldSortableHeader())
                ->addComponent(new GridFieldDataColumns())
                ->addComponent(new GridFieldPaginator(50))
                ->addComponent(new GridFieldEditButton())
                ->addComponent(new GridFieldDeleteAction())
                ->addComponent(new GridFieldDetailForm())
                ->addComponent(new GridFieldFilterHeader())
        //->addComponent(new GridFieldOrderableRows('SortOrder'))
        );

        $dataColumns = $config->getComponentByType('GridFieldDataColumns');
        $dataColumns->setFieldCasting(array(
            'getColor' => 'HTMLText->RAW'
        ));

        $BrandColorTypesField = new GridField(
            'BrandColorTypes',
            'BrandColorTypes',
            $this->owner->BrandColorTypes(),
            GridFieldConfig::create()
                ->addComponent(new GridFieldToolbarHeader())
                ->addComponent(new GridFieldAddNewButton("toolbar-header-right"))
                ->addComponent(new GridFieldSortableHeader())
                ->addComponent(new GridFieldDataColumns())
                ->addComponent(new GridFieldPaginator(50))
                ->addComponent(new GridFieldEditButton())
                ->addComponent(new GridFieldDeleteAction())
                ->addComponent(new GridFieldDetailForm())
                ->addComponent(new GridFieldFilterHeader())
        //->addComponent(new GridFieldOrderableRows('SortOrder'))
        );
        $OwnIpsGridField = new GridField(
            'OwnIps',
            _t("SiteConfig.OWN_IPS", "Own ip's"),
            $this->owner->OwnIps(),
            GridFieldConfig::create()
                ->addComponent(new GridFieldToolbarHeader())
                ->addComponent(new GridFieldAddNewButton("toolbar-header-right"))
                ->addComponent(new GridFieldSortableHeader())
                ->addComponent(new GridFieldDataColumns())
                ->addComponent(new GridFieldPaginator(50))
                ->addComponent(new GridFieldEditButton())
                ->addComponent(new GridFieldDeleteAction())
                ->addComponent(new GridFieldDetailForm())
                ->addComponent(new GridFieldFilterHeader())
        //->addComponent(new GridFieldOrderableRows('SortOrder'))
        );
        $OwnIpsInfoField = LiteralField::create('OwnIpsInfo', _t("OwnIp.OWN_IP_INFO", "Own ip adresses can be used for exclude functions like Analytics, dependent on your site configuration."));

        /*$conf=GridFieldConfig_RelationEditor::create(10);
        $conf->addComponent(new GridFieldOrderableRows('SortOrder'));
        $conf->removeComponentsByType('GridFieldPaginator');
        $conf->removeComponentsByType('GridFieldPageCount');*/

        $GlobalFromEmailField = EmailField::create("GlobalFromEmail", _t("SiteConfig.GLOBAL_FROM_EMAIL","Global from email"));
        $GlobalFromNameField = TextField::create("GlobalFromName", _t("SiteConfig.GLOBAL_FROM_NAME","Global from name"));
        $GlobalFromNameField->setDescription(_t("SiteConfig.GLOBAL_FROM_NAME_DESCRIPTION","The name which is displayed with the from email"));

        $GlobalContentInfoField = LiteralField::create('GlobalContentInfoField', _t("SiteConfig.GLOBALCONTENTINFO","text"));
        $FooterCmsHeaderField = HeaderField::create('FooterCmsHeaderField', 'Footer');
        $FooterContentField = HtmlEditorField::create("FooterContent", _t("SiteConfig.FooterContent","Footer text 1"));
        $FooterContentField->setRows(15);
        $FooterContent2Field = HtmlEditorField::create("FooterContent2", _t("SiteConfig.FOOTERCONTACT","Footer text 2"));
        $FooterContent2Field->setRows(15);
        $FooterContent3Field = HtmlEditorField::create("FooterContent3", _t("SiteConfig.FOOTERCONTACT","Footer text 3"));
        $FooterContent3Field->setRows(15);
        $FooterContent4Field = HtmlEditorField::create("FooterContent4", _t("SiteConfig.FOOTERCONTACT","Footer text 4"));
        $FooterContent4Field->setRows(15);
        $FooterColumnTitle1Field = TextField::create("FooterColumnTitle1", _t("SiteConfig.FOOTERCOLUMNTITLE","Column title")." 1");
        $FooterColumnTitle2Field = TextField::create("FooterColumnTitle2", _t("SiteConfig.FOOTERCOLUMNTITLE","Column title")." 2");
        $FooterColumnTitle3Field = TextField::create("FooterColumnTitle3", _t("SiteConfig.FOOTERCOLUMNTITLE","Column title")." 3");
        $FooterColumnTitle4Field = TextField::create("FooterColumnTitle4", _t("SiteConfig.FOOTERCOLUMNTITLE","Column title")." 4");

        //$googeanalyticsinfofield = LiteralField::create("googeanalyticsinfofield", "<h2>Google Analytics</h2>");
        $gatrackingcodefield = TextField::create("GATrackingCode", "Google Analytics tracking code");
        $facebookappidfield = TextField::create("FacebookAppId", "Facebook app ID");
        $tawksiteidfield = TextField::create("TawkSiteId", "Tawk site ID");

        $socialmediainfofield = LiteralField::create("socialmediainfofield", _t("SiteConfig.SOCIALMEDIAINFO","Start the URL's with <strong>http://</strong> or <strong>https://</strong>."));
        $facebookurlfield = TextField::create("FacebookUrl", "Facebook URL");
        $twitterurlfield = TextField::create("TwitterUrl", "Twitter URL");
        $instagramurlfield = TextField::create("InstagramUrl", "Instagram URL");
        $youtubeurlfield = TextField::create("YoutubeUrl", "Youtube URL");
        $linkedinurlfield = TextField::create("LinkedinUrl", "LinkedIn URL");
        $pinteresturlfield = TextField::create("PinterestUrl", "Pinterest URL");
        $googleplusurlfield = TextField::create("GoogleplusUrl", "Google+ URL");
        $snapchaturlfield = TextField::create("SnapchatUrl", "Snapchat URL");
        $soundcloudfield = TextField::create("SoundcloudUrl", "Soundcloud URL");

        $fields->addFieldsToTab("Root.Main", array(
            $GlobalFromEmailField,
            $GlobalFromNameField
        ));

        $fields->addFieldsToTab("Root.Content", array(
            $GlobalContentInfoField,
            $FooterCmsHeaderField,
            $FooterColumnTitle1Field,
            $FooterContentField,
            $FooterColumnTitle2Field,
            $FooterContent2Field,
            $FooterColumnTitle3Field,
            $FooterContent3Field,
            $FooterColumnTitle4Field,
            $FooterContent4Field
        ));

        $fields->addFieldsToTab("Root."._t("SiteConfig.EXTRA_NAVIGATION", "Extra Navigation"), array(
            $ExtraNavigationItemsField
        ));

        if (Permission::check('ADMIN')) {
            $fields->addFieldsToTab("Root." . _t("SiteConfig.CONNECTIONS", "Connections"), array(
                $gatrackingcodefield,
                $facebookappidfield,
                $tawksiteidfield
            ));
        }

        $fields->addFieldsToTab("Root."._t("SiteConfig.SOCIAL_MEDIA", "Social Media"), array(
            $socialmediainfofield,
            $facebookurlfield,
            $twitterurlfield,
            $instagramurlfield,
            $youtubeurlfield,
            $linkedinurlfield,
            $pinteresturlfield,
            $googleplusurlfield,
            $snapchaturlfield,
            $soundcloudfield
        ));

        if (Permission::check('ADMIN')) {
            $fields->addFieldsToTab("Root." . _t("SiteConfig.OWN_IPS", "Own ip's"), array(
                $OwnIpsInfoField,
                $OwnIpsGridField
            ));

            /*$fields->addFieldsToTab("Root.BrandColors", array(
                $BrandColorsField
            ));*/

            $fields->addFieldToTab("Root." . _t("SiteConfig.BRAND_COLORS", "Colors"), new TabSet('ContentBlockSet',
                /*new Tab('BrandColors', array(
                    $BrandColorsField
                ))*/
                new Tab('BrandColors', $BrandColorsField),
                new Tab('BrandColorTypes', $BrandColorTypesField)
            ));
        }

    }

    /*public function validate() {
        $result = parent::validate();
        if(strlen($this->Label) > 28 || strlen($this->Label) < 2) {
            $result->error(_t("ExtraNavigation.LABEL_VALIDATION","The label must be between 2 and 28 characters (include spaces)."));
        }
        if(!filter_var($this->ExternLink, FILTER_VALIDATE_URL) && !empty($this->ExternLink)) {
            $result->error(_t("ExtraNavigation.EXTERNAL_LINK_VALIDATION","External link"));
        }
        return $result;
    }*/

    public function GlobalFromEmail() {

        if ($output = $this->owner->GlobalFromEmail){
            return $output;
        }else{
            return "noreply@hestec.email";
        }

    }

    public function CheckFacebookUrl() {

        if (filter_var($this->owner->FacebookUrl, FILTER_VALIDATE_URL)){
            return true;
        }else{
            return false;
        }

    }

    public function CheckTwitterUrl() {

        if (filter_var($this->owner->TwitterUrl, FILTER_VALIDATE_URL)){
            return true;
        }else{
            return false;
        }

    }

    public function CheckInstagramUrl() {

        if (filter_var($this->owner->InstagramUrl, FILTER_VALIDATE_URL)){
            return true;
        }else{
            return false;
        }

    }

    public function CheckYoutubeUrl() {

        if (filter_var($this->owner->YoutubeUrl, FILTER_VALIDATE_URL)){
            return true;
        }else{
            return false;
        }

    }

    public function CheckLinkedinUrl() {

        if (filter_var($this->owner->LinkedinUrl, FILTER_VALIDATE_URL)){
            return true;
        }else{
            return false;
        }

    }

    public function CheckPinterestUrl() {

        if (filter_var($this->owner->PinterestUrl, FILTER_VALIDATE_URL)){
            return true;
        }else{
            return false;
        }

    }

    public function CheckGoogleplusUrl() {

        if (filter_var($this->owner->GoogleplusUrl, FILTER_VALIDATE_URL)){
            return true;
        }else{
            return false;
        }

    }

    public function CheckSnapchatUrl() {

        if (filter_var($this->owner->SnapchatUrl, FILTER_VALIDATE_URL)){
            return true;
        }else{
            return false;
        }

    }

    public function CheckSoundcloudUrl() {

        if (filter_var($this->owner->SoundcloudUrl, FILTER_VALIDATE_URL)){
            return true;
        }else{
            return false;
        }

    }

    public function isNotOwnIp() {

        if (OwnIp::get()->filter('IpAddress', $_SERVER['REMOTE_ADDR'])->count() == 0){
            return true;
        }else{
            return false;
        }

    }

}