<?php

class CMSHelpLink extends DataExtension {
	
	function init() {
        CMSMenu::remove_menu_item('Help');
        CMSMenu::add_link('Help',"Help","https://www.hestecsupport.nl/index.php?/Knowledgebase/List/Index/4/silverstripe-cms".Config::inst()->get($this->class, 'url'),-2,array("target"=>"_blank"));
	}
	
}