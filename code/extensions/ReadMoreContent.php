<?php
class ReadMoreContent extends DataExtension {

    private static $db = array(
        'ReadMoreContent' => 'HTMLText'
    );

    private static $has_one = array(
    );

    public function updateCMSFields(FieldList $fields) {

        $readmorefield = HtmlEditorField::create('ReadMoreContent', _t('SiteTree.READMORE', 'Read more'))
            ->setDescription(_t('SiteTree.READMORE-HELP', 'If there is content in this Read more field a "read more" link will added after the first content.'));

        $readmoretogglefield = new ToggleCompositeField('ReadMoreContentToggle', _t('SiteTree.READMORE', 'Read more'), array(
            $readmorefield
        ));
        $readmoretogglefield->setStartClosed(true);



        $fields->addFieldToTab('Root.Main', $readmoretogglefield, 'Metadata');

        return $fields;

    }

}
