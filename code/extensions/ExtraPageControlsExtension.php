<?php

class ExtraPageControlsExtension extends DataExtension {

    // Detect tablets & mobile phones
    public function isMobile(){
        $detect = new Mobile_Detect;
        // Any mobile device (phones or tablets).
        if ( $detect->isMobile() ) {
            return true;
        }else{
            return false;
        }
    }

    // Detect tablets
    public function isTablet(){
        $detect = new Mobile_Detect;
        // Any tablet device.
        if( $detect->isTablet() ){
            return true;
        }else{
            return false;
        }
    }
    // Detect tablets & mobile phones
    public function isPhone(){
        $detect = new Mobile_Detect;
        // Exclude tablets.
        if( $detect->isMobile() && !$detect->isTablet() ){
            return true;
        }else{
            return false;
        }
    }

    // Detect ifDev
    public function isDev(){
        // Exclude tablets.
        if( Director::isDev() ){
            return true;
        }else{
            return false;
        }
    }

    // Detect ifLive
    public function isLive(){
        // Exclude tablets.
        if( Director::isLive() ){
            return true;
        }else{
            return false;
        }
    }

    // TimeVersion used to give Less.js (included in themes/templates/Page.ss) a version number
    public function TimeVersion(){
        return time();
    }

    // get the language in the template with $LangFromLocale for example: gets "en" from "en_US"
    public function getLangFromLocale() {
        return i18n::get_lang_from_locale(i18n::get_locale());
    }

    public function CanonicalUrl(){

        if (filter_var(CANONICAL_BASE_URL, FILTER_VALIDATE_URL)) {
            return CANONICAL_BASE_URL.$_SERVER['REQUEST_URI'];
        }else{
            return false;
        }

    }

    // geoip functie gaf deze fout bij en dev/build:
    //[Tue Sep 27 15:17:55 2016] [error] [client 90.145.111.13] PHP Parse error:  syntax error, unexpected '[' in /home/w4l39/domains/39.w4l.nl/public_html/zzz_admin/code/extensions/ExtraPageControlsExtension.php on line 72

    /*public function getGeoipCountryCode() {

        if (function_exists('geoip_record_by_name')) {

            //return geoip_record_by_name('52.63.2.69')['country_code']; // test, this ip is from Australia, so must return: AU
            return geoip_record_by_name($_SERVER['REMOTE_ADDR'])['country_code'];

        }else{

            return false; // geoip library not installed in PHP, see Hestec wiki how to install

        }

    }*/

}
