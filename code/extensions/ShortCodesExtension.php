<?php
class ShortCodesExtension extends DataExtension {

    public function AddToAnyHandler() {

        $addtoanycode = '<!-- AddToAny BEGIN -->
<div class="a2a_kit a2a_default_style">
<a class="a2a_dd" href="http://www.addtoany.com/share_save">Delen</a>
<span class="a2a_divider"></span>
<a class="a2a_button_facebook"></a>
<a class="a2a_button_twitter"></a>
<a class="a2a_button_google_plus"></a>
<a class="a2a_button_linkedin"></a>
<a class="a2a_button_email"></a>
</div>
<script type="text/javascript" src="//static.addtoany.com/menu/page.js"></script>
<!-- AddToAny END -->';

        return $addtoanycode;
    }

    public static function FaHandler($arguments, $content) {

        $output = "<span class=\"icon fa ".$arguments['icon']."\"></span>";

        return $output;

    }

}
