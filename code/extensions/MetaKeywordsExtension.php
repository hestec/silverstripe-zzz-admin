<?php

class MetaKeywordsExtension extends DataExtension {

	private static $db = array(
		"MetaKeywords" => "Varchar(100)",
	);

    public function updateCMSFields(FieldList $fields) {
        $metaData = $fields->fieldByName('Root.Main.Metadata');

        $metaFieldTitle = new TextField("MetaKeywords", $this->owner->fieldLabel('MetaKeywords', "", "100"));
        $metaFieldTitle->setRightTitle(
            _t(
                'SiteTree.METAKEYWORDSHELP',
                'Shown at the top of the browser window and used as the "linked text" by search engines.'
            )
        )->addExtraClass('help');

        $metaData->insertAfter($metaFieldTitle, 'MetaDescription');

        return $fields;
    }

    public function updateFieldLabels(&$labels) {
        $labels['MetaKeywords'] = _t('SiteTree.METAKEYWORDS', "Keywords");
    }

    public function MetaTags(&$tags) {
        if($this->owner->MetaKeywords) {
            $tags .= "<meta name=\"keywords\" content=\"" . Convert::raw2att($this->owner->MetaKeywords) . "\" />\n";
        }
    }

}