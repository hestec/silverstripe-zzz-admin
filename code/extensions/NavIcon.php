<?php
class NavIcon extends DataExtension {

    private static $db = array(
        'NavIcon' => 'Varchar(255)'
    );

    private static $has_one = array(
    );

    /*private static $defaults = array(
        'NavIcon' => 'fa-'
    );*/

    public function updateCMSFields(FieldList $fields) {

        $NavIconField = FontAwesomeIconPickerField::create('NavIcon', _t('SiteTree.NAVICON', 'Navigation icon'));

        $fields->addFieldToTab('Root.Main', $NavIconField, 'Content');

        return $fields;

    }

}
