<?php

// cms branding
LeftAndMain::require_css('zzz_admin/css/hestec-cms-branding.css');
LeftAndMain::set_application_link('http://www.hestec.nl');
LeftAndMain::setApplicationName('Hestec CMS');

// TinyMCE Tweaks
HtmlEditorConfig::get('cms')->setOption('paste_text_sticky', 'true');
HtmlEditorConfig::get('cms')->setOption('paste_text_sticky_default', 'true');

Object::add_extension('SiteConfig', 'CustomSiteConfig');

ShortcodeParser::get('default')->register(
    'AddToAny', array('ShortCodesExtension', 'AddToAnyHandler')
);
ShortcodeParser::get('default')->register(
    'fa', array('ShortCodesExtension', 'FaHandler')
);
